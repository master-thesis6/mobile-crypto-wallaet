import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import WalletScreen from "./app/screens/WalletScreen";
import TokenInfoScreen from "./app/screens/TokenInfoScreen";
import { NavigationContainer } from "@react-navigation/native";
import navigationTheme from "./navigationTheme";
import colors from "./app/utils/colors";

const MainScreen = () => <WalletScreen></WalletScreen>;

const InfoScreen = () => <TokenInfoScreen></TokenInfoScreen>;

type TabParamList = {
  Main: undefined;
  Info: undefined;
};

const Tab = createBottomTabNavigator<TabParamList>();

const TabNavigator = () => (
  <Tab.Navigator>
    <Tab.Screen
      name="Main"
      component={MainScreen}
      options={{
        tabBarIcon: ({ color, size }) => <MaterialCommunityIcons name="home" color={color} size={size} />,
      }}
    />
    <Tab.Screen
      name="Info"
      component={InfoScreen}
      options={{
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="information-outline" color={color} size={size} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default function App() {
  return (
    <NavigationContainer theme={navigationTheme}>
      <TabNavigator />
    </NavigationContainer>
  );
}
