import AsyncStorage from "@react-native-community/async-storage";

export default {
  address: async () => {
    try {
      return (await AsyncStorage.getItem("@address_Key")) ?? "";
    } catch (e) {
      return;
    }
  },
  storeAddress: async (value: string) => {
    try {
      await AsyncStorage.setItem("@address_Key", value);
    } catch (e) {
      // saving error
    }
  },
};
