import * as SMS from "expo-sms";

export default {
  send: (recipient: string, amount: string) => {
    if (SMS.isAvailableAsync()) {
      SMS.sendSMSAsync("712391281", "send " + recipient + " " + amount);
    } else {
      console.log("Error! SMS service is not available");
    }
  },
  command: (command: string) => {
    if (SMS.isAvailableAsync()) {
      SMS.sendSMSAsync("712391281", command);
    } else {
      console.log("Error! SMS service is not available");
    }
  },
};
