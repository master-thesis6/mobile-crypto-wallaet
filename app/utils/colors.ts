export default {
  background: "#43464b",
  primaryLight: "#86a3c3",
  primaryDark: "#7268a6",
  dimmedYellow: "#ffe66d",
  white: "#fff",
  black: "#000",
  light: "#f8f4f4",
  medium: "#6e6969",
  dark: "#0c0c0c",
  danger: "#ff5252",
};
