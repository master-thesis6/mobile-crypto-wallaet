export interface GenericPickerItem {
  backgroundColor?: string;
  icon?: string;
  label: string;
  value: number;
}
