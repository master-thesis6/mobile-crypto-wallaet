import React from "react";
import { View, StyleSheet, Text } from "react-native";
import colors from "../../utils/colors";

interface Props {
  error?: string;
  visible?: boolean;
}

function FormErrorMessage(props: Props) {
  const { error, visible } = props;
  if (!visible || !error) return null;
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{error}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 5,
    marginHorizontal: 15,
  },
  text: {
    fontSize: 16,
    color: colors.danger,
  },
});

export default FormErrorMessage;
