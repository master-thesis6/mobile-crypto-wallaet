import React from "react";
import { useFormikContext } from "formik";

import colors from "../../utils/colors";
import RoundedButton from "../RoundedButton";

interface Props {
  title: string;
}

function SubmitButton(props: Props) {
  const { handleSubmit } = useFormikContext();
  const { title } = props;

  return <RoundedButton color="#b755ff" buttonText={title} onPress={handleSubmit} />;
}

export default SubmitButton;
