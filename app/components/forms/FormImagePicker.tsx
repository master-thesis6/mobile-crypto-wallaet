import React from "react";
import { View, StyleSheet } from "react-native";
import { useFormikContext } from "formik";
import FormErrorMessage from "./FormErrorMessage";
import ImageInputList from "../ImageInputList";

interface Props {
  name: string;
}

function FormImagePicker(props: Props) {
  const { name } = props;
  const { setFieldValue, touched, values, errors } = useFormikContext<any>();
  const imageUris: string[] = values[name];

  const handleAdd = (uri: string) => {
    setFieldValue(name, [...imageUris, uri]);
  };
  const handleRemove = (uri: string) => {
    setFieldValue(
      name,
      imageUris.filter((image) => image !== uri)
    );
  };

  return (
    <>
      <ImageInputList imageUris={values[name]} onAddImage={handleAdd} onRemoveImage={handleRemove} />
      <FormErrorMessage visible={Boolean(touched[name])} error={errors[name]?.toString()} />
    </>
  );
}

export default FormImagePicker;
