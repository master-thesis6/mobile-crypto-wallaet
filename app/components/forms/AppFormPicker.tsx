import React from "react";
import { StyleSheet } from "react-native";
import { useFormikContext } from "formik";

import { FormErrorMessage } from "./index";
import AppPicker from "../picker/AppPicker";
import { GenericPickerItem } from "../../models";

interface Props {
  icon?: string;
  name: string;
  numOfColumns?: number;
  placeholder: string;
  items: GenericPickerItem[];
  width?: number | string;
  PickerItemComponent: any;
}

function AppFormPicker(props: Props) {
  const { name, icon, numOfColumns, placeholder, items, width, PickerItemComponent } = props;
  const { setFieldValue, values, touched, errors } = useFormikContext<any>();
  return (
    <>
      <AppPicker
        icon={icon}
        placeHolder={placeholder}
        width={width}
        items={items}
        numOfColumns={numOfColumns}
        PickerItemComponent={PickerItemComponent}
        onItemSelected={(item) => setFieldValue(name, item)}
        selectedItem={values[name]}
      />
      <FormErrorMessage visible={Boolean(touched[name])} error={errors[name]?.toString()} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {},
});

export default AppFormPicker;
