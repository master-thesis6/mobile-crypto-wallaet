import React from "react";
import { TextInputIOSProps, TextInputProps } from "react-native";
import { useFormikContext } from "formik";

import AppTextInput from "../AppTextInput";
import FormErrorMessage from "./FormErrorMessage";

interface Props {
  name: string;
  icon?: string;
  otherProps?: TextInputProps | TextInputIOSProps;
  width?: number | string;
}

function AppFormField(props: Props) {
  const { name, icon, otherProps, width } = props;
  const { setFieldTouched, handleChange, touched, errors, values } = useFormikContext<any>();

  return (
    <>
      <AppTextInput
        icon={icon}
        width={width}
        value={String(values[name])}
        styles={{
          onBlur: () => setFieldTouched(name),
          onChangeText: handleChange(name),
          ...otherProps,
        }}
      />
      <FormErrorMessage visible={Boolean(touched[name])} error={errors[name]?.toString()} />
    </>
  );
}

export default AppFormField;
