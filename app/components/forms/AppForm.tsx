import React, { ReactNode } from "react";
import { Formik } from "formik";
import { ObjectSchema } from "yup";
import { View } from "react-native";

interface Props<T> {
  initialValues: T;
  onSubmit: (values: T) => void;
  validationSchema: ObjectSchema;
  children: ReactNode;
}

function AppForm<T>(props: Props<T>, children: ReactNode) {
  return (
    <View style={{ padding: 10 }}>
      <Formik initialValues={props.initialValues} onSubmit={props.onSubmit} validationSchema={props.validationSchema}>
        {() => <>{props.children}</>}
      </Formik>
    </View>
  );
}

export default AppForm;
