import React, { ReactNode } from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import Constants from "expo-constants";

function Screen(props: { children: ReactNode; color?: string; padding?: number }) {
  return (
    <SafeAreaView
      style={[styles.container, { backgroundColor: props.color ?? "#fff", paddingHorizontal: props.padding ?? 0 }]}
    >
      {props.children}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    paddingTop: Constants.statusBarHeight,
  },
});

export default Screen;
