import React from "react";
import { View, StyleSheet, TouchableOpacity, TouchableHighlight } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import AppText from "./AppText";
import colors from "../utils/colors";

interface Props {
  icon: string;
  text: string;
  onPress: () => void;
}

function CommandItem(props: Props) {
  return (
    <TouchableHighlight
      style={styles.container}
      onPress={props.onPress}
      activeOpacity={0.7}
      underlayColor={colors.primaryDark}
    >
      <LinearGradient
        // Button Linear Gradient
        colors={["#b755ff", "#584bdd"]}
        start={[0, 1]}
        end={[1, 0]}
        style={{
          alignContent: "center",
          alignItems: "center",
          width: "100%",
          height: "100%",
          borderRadius: 15,
        }}
      >
        {/* <MaterialCommunityIcons name={props.icon} size={50} color={colors.dark} /> */}
        <AppText text={props.text} style={styles.text} />
      </LinearGradient>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primaryLight,
    marginVertical: 15,
    marginHorizontal: 18,
    width: "40%",
    height: 45,
    borderRadius: 15,
  },
  text: {
    fontSize: 20,
    marginTop: 9,
    color: colors.white,
  },
});

export default CommandItem;
