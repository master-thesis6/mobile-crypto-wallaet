import React from "react";
import { StyleSheet, TouchableOpacity, Text } from "react-native";

function RoundedButton(props: { buttonText: string; color: string; onPress: () => void }) {
  return (
    <TouchableOpacity onPress={props.onPress} style={[styles.container, { backgroundColor: props.color }]}>
      <Text style={styles.text}>{props.buttonText}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    borderRadius: 25,
    marginVertical: 10,
  },
  text: {
    color: "#fff",
    fontSize: 18,
    fontWeight: "600",
    textTransform: "uppercase",
  },
});

export default RoundedButton;
