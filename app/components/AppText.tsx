import React, { ReactNode } from "react";
import { View, StyleSheet, Text, Platform, TextStyle } from "react-native";

interface Props {
  text: String;
  style?: TextStyle;
}
function AppText({ text, style }: Props) {
  return <Text style={[styles.container, style]}>{text}</Text>;
}

const styles = StyleSheet.create({
  container: {
    fontSize: 18,
    fontFamily: Platform.OS == "android" ? "Roboto" : "Avenir",
  },
});

export default AppText;
