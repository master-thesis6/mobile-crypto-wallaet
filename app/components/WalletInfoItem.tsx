import React from "react";
import { View, StyleSheet } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import AppText from "./AppText";
import colors from "../utils/colors";

interface Props {
  icon: string;
  title: string;
  description: string;
}
function WalletInfoItem(props: Props) {
  const { icon, title, description } = props;
  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginBottom: 8,
        }}
      >
        <MaterialCommunityIcons name={icon} size={30} color={colors.medium} />
        <AppText text={title} style={{ fontSize: 20, fontWeight: "400", color: colors.dark, marginLeft: 40 }} />
      </View>
      <AppText text={description} style={{ fontSize: 18, fontWeight: "400", color: colors.medium, marginLeft: 75 }} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 15,
    marginHorizontal: 25,
  },
});

export default WalletInfoItem;
