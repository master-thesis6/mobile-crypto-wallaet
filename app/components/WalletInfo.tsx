import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import AppText from "./AppText";
import colors from "../utils/colors";

interface Props {
  title: String;
  description: String;
  isEditable?: boolean;
  onClick: () => void;
  onCopy?: () => void;
}

function WalletInfo(props: Props) {
  const { title, description, isEditable } = props;
  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <AppText
          text={title}
          style={{ fontSize: 22, fontWeight: "300", marginRight: 5, color: colors.white, marginBottom: 5 }}
        />
        {isEditable && (
          <TouchableOpacity onPress={props.onClick}>
            <MaterialCommunityIcons name="circle-edit-outline" size={23} color={colors.white} />
          </TouchableOpacity>
        )}
      </View>
      <View style={styles.titleContainer}>
        <AppText text={description} style={{ fontSize: 12, marginRight: 10, marginLeft: 5, color: colors.white }} />
        {description != "" && (
          <TouchableOpacity>
            <MaterialCommunityIcons name="content-copy" size={20} color={colors.white} />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 5,
    width: "100%",
  },
  titleContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
});

export default WalletInfo;
