import React from "react";
import { View, TextInput, StyleSheet, Platform, TextInputIOSProps, TextInputProps } from "react-native";

import { MaterialCommunityIcons } from "@expo/vector-icons";
import colors from "../utils/colors";

interface Props {
  icon?: string;
  styles?: TextInputProps | TextInputIOSProps;
  width?: number | string;
  value?: string;
}

function AppTextInput(props: Props) {
  const { icon, width } = props;

  return (
    <View style={[styles.container, { width: width ?? "100%" }]}>
      {icon && <MaterialCommunityIcons name={icon} color={colors.medium} size={20} style={{ marginRight: 10 }} />}
      <TextInput placeholderTextColor={colors.medium} {...props.styles} style={styles.text} value={props.value} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    padding: 15,
    backgroundColor: colors.light,
    alignItems: "center",
    borderRadius: 25,
    marginVertical: 10,
  },
  text: {
    color: colors.dark,
    width: "100%",
    fontSize: 18,
    fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
  },
});

export default AppTextInput;
