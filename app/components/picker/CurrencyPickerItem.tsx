import React from "react";
import { View, StyleSheet, TouchableHighlight } from "react-native";
import AppText from "../AppText";
import { GenericPickerItem } from "../../models";
import colors from "../../utils/colors";

interface Props {
  item: GenericPickerItem;
  onPress: () => void;
}

function CurrencyPickerItem(props: Props) {
  const { item, onPress } = props;
  return (
    <TouchableHighlight style={styles.container} onPress={onPress} activeOpacity={0.7} underlayColor={colors.light}>
      <AppText text={item.label} style={{ fontSize: 20 }} />
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
});

export default CurrencyPickerItem;
