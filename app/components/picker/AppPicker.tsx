import React, { useState, ReactNode } from "react";
import { View, StyleSheet, Text, TouchableWithoutFeedback, Modal, Button, FlatList } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import colors from "../../utils/colors";
import Screen from "../Screen";
import { GenericPickerItem } from "../../models";

interface Props {
  icon?: string;
  items: GenericPickerItem[];
  numOfColumns?: number;
  onItemSelected: (item: GenericPickerItem) => void;
  PickerItemComponent: any;
  placeHolder?: string;
  selectedItem?: GenericPickerItem;
  width?: number | string;
}

function AppPicker(props: Props) {
  const { icon, placeHolder, items, selectedItem, numOfColumns, onItemSelected, PickerItemComponent, width } = props;
  const [isModalVisible, setModalVisible] = useState(false);

  const onItemPicked = (item: GenericPickerItem) => {
    setModalVisible(false);
    onItemSelected(item);
  };

  return (
    <>
      <TouchableWithoutFeedback onPress={() => setModalVisible(true)}>
        <View style={[styles.container, { width: width ?? "100%" }]}>
          {icon && <MaterialCommunityIcons name={icon} color={colors.medium} size={20} style={{ marginRight: 10 }} />}
          <Text style={[styles.text, { color: selectedItem ? colors.dark : colors.medium }]}>
            {selectedItem ? selectedItem.label : placeHolder}
          </Text>
          <MaterialCommunityIcons name="chevron-down" color={colors.medium} size={20} />
        </View>
      </TouchableWithoutFeedback>
      <Modal visible={isModalVisible} animationType="slide">
        <Screen>
          <Button title="Close" onPress={() => setModalVisible(false)} />
          <FlatList
            data={items}
            numColumns={numOfColumns}
            keyExtractor={(item) => item.value.toString()}
            renderItem={({ item }) => <PickerItemComponent item={item} onPress={() => onItemPicked(item)} />}
          />
        </Screen>
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    padding: 15,
    marginVertical: 10,
    backgroundColor: colors.light,
    borderRadius: 25,
  },
  text: {
    color: colors.dark,
    fontSize: 18,
    flex: 1,
  },
});

export default AppPicker;
