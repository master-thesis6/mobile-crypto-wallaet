import React, { useState, useEffect } from "react";
import { View, StyleSheet, TouchableOpacity, Alert } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Clipboard from "@react-native-community/clipboard";

import storage from "../utils/storage";
import AppText from "./AppText";
import colors from "../utils/colors";
import WalletInfo from "./WalletInfo";

function WalletHeader() {
  const [wallet, setAddress] = useState("");

  useEffect(() => {
    storage.address().then((value) => {
      setAddress(value?.toString() ?? "");
    });
  });

  const setNewAddress = (address: string) => {
    if (address === "") {
      return;
    }
    storage.storeAddress(address);
    setAddress(address);
  };

  const writeToClipboard = () => {
    Clipboard.setString(wallet);
  };

  const showAlert = () => {
    Alert.prompt("Please provide your wallet address.", "", [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      { text: "OK", onPress: (walletAddress) => setNewAddress(walletAddress ?? "") },
    ]);
  };

  return (
    <LinearGradient
      // Button Linear Gradient
      colors={["#b755ff", "#584bdd"]}
      start={[0, 1]}
      end={[1, 0]}
      style={{
        width: "100%",
        height: "32%",
        borderBottomRightRadius: 40,
      }}
    >
      <View
        style={{
          padding: 25,
          paddingTop: 40,
          alignContent: "center",
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center", marginBottom: 10 }}>
          <AppText
            text="Dundies wallet"
            style={{ fontSize: 28, fontWeight: "400", color: colors.white, marginRight: 5 }}
          />
        </View>

        <WalletInfo
          title="Address"
          onClick={showAlert}
          onCopy={writeToClipboard}
          description="0x915e85a72C5e3522fb35c640Dc24fe14511f4caa"
          isEditable={true}
        />
      </View>

      <View
        style={{
          width: "100%",
          flex: 1,
          backgroundColor: colors.light,
          borderTopLeftRadius: 100,
          borderTopRightRadius: 100,
          shadowOpacity: 0.3,
          shadowRadius: 10,
        }}
      />
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  container: {},
});

export default WalletHeader;
