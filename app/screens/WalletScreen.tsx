import React, { useState } from "react";
import { View, StyleSheet, Text, FlatList, ScrollView, Modal, Button, Dimensions } from "react-native";
import { LineChart } from "react-native-chart-kit";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import AppText from "../components/AppText";
import CommandItem from "../components/CommandItem";
import colors from "../utils/colors";
import SendInfoScreen from "./SendInfoScreen";
import SMSSender from "../utils/sms-sender";
import WalletHeader from "../components/WalletHeader";

const DATA = [
  {
    id: 1,
    title: "Address",
    command: "address",
  },
  {
    id: 2,
    title: "Balance",
    command: "balance",
  },
  {
    id: 3,
    title: "Send",
    command: "send",
  },
  {
    id: 4,
    title: "History",
    command: "history",
  },
];

function WalletScreen() {
  const [isModalVisible, setModalVisible] = useState(false);

  const handleCommandPress = (name: string) => {
    if (name != "Send") {
      SMSSender.command(name.toLowerCase());
      return;
    }
    setModalVisible(true);
  };
  return (
    <>
      <View style={styles.container}>
        <WalletHeader />
        <View style={{ flexDirection: "row", alignItems: "center", marginBottom: 25 }}>
          <MaterialCommunityIcons name="ethereum" size={28} color={colors.dark} style={{ marginRight: 5 }} />
          <AppText text="Ethereum price (in USD)" style={{ fontSize: 23, fontWeight: "400", color: colors.dark }} />
        </View>

        <LineChart
          data={{
            labels: ["Jul 29", "Jul 30", "Jul 31", "Aug 1", "Aug 2", "Aug 3", "Aug 4"],
            datasets: [
              {
                data: [395, 388, 379, 368, 342, 327, 321].reverse(),
              },
            ],
          }}
          width={Dimensions.get("window").width} // from react-native
          height={170}
          chartConfig={{
            backgroundGradientFrom: colors.light,
            backgroundGradientTo: colors.light,
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            style: {
              borderRadius: 16,
            },
          }}
          bezier
          style={{
            marginVertical: 8,
            borderRadius: 16,
            marginBottom: 20,
          }}
        />
        <FlatList
          data={DATA}
          renderItem={(item) => (
            <CommandItem
              text={item.item.title}
              icon={item.item.title}
              onPress={() => handleCommandPress(item.item.title)}
            />
          )}
          numColumns={2}
        />
      </View>

      <Modal visible={isModalVisible} animationType="slide">
        <SendInfoScreen onClose={() => setModalVisible(false)} />
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    backgroundColor: colors.light,
  },
  title: {
    fontSize: 32,
  },
});

export default WalletScreen;
