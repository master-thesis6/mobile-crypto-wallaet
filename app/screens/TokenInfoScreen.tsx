import React from "react";
import { View, StyleSheet, FlatList } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import colors from "../utils/colors";
import AppText from "../components/AppText";
import WalletInfoItem from "../components/WalletInfoItem";

const DATA = [
  {
    id: 1,
    icon: "weight",
    title: "Total supply",
    desciption: "10000000000",
  },
  {
    id: 2,
    icon: "cash-multiple",
    title: "Rate",
    desciption: "DND = 0.000000001 ETH",
  },
  {
    id: 3,
    icon: "home-currency-usd",
    title: "Crowdsale address",
    desciption: "109kldasj912dasj-992112sad",
  },
];

function TokenInfoScreen() {
  return (
    <LinearGradient
      // Button Linear Gradient
      colors={["#b755ff", "#584bdd"]}
      start={[1, 1]}
      end={[0, 0]}
      style={{
        alignContent: "center",
        alignItems: "center",
        width: "100%",
        height: "100%",
      }}
    >
      <View style={styles.container}>
        <MaterialCommunityIcons
          name="coin"
          size={60}
          color={colors.white}
          style={{ marginBottom: 10, marginTop: 20 }}
        />
        <AppText text="Dundies Token Info" style={{ fontSize: 22, fontWeight: "700", color: colors.white }} />
      </View>

      <View style={styles.infoContainer}>
        <FlatList
          data={DATA}
          keyExtractor={(item) => item.id.toString()}
          renderItem={(item) => (
            <WalletInfoItem icon={item.item.icon} title={item.item.title} description={item.item.desciption} />
          )}
        />
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 25,
    paddingTop: 40,
    alignItems: "center",
  },
  infoContainer: {
    marginTop: 25,
    backgroundColor: colors.light,
    alignContent: "center",
    width: "100%",
    paddingTop: 50,
    paddingBottom: 50,
    borderTopRightRadius: 50,
    borderBottomLeftRadius: 50,
    shadowOpacity: 0.4,
    shadowRadius: 15,
    elevation: 10,
  },
});

export default TokenInfoScreen;
