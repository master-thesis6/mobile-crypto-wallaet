import React from "react";
import { StyleSheet, Button } from "react-native";
import { string, object, number } from "yup";

import Screen from "../components/Screen";
import { AppForm, AppFormField, SubmitButton } from "../components/forms";
import AppText from "../components/AppText";
import colors from "../utils/colors";
import SMSSender from "../utils/sms-sender";

interface Props {}

interface FormValues {
  addressOrPhone: string;
  amount: number;
}

const validationSchema = object().shape({
  addressOrPhone: string().required(),
  amount: number().required().moreThan(10).integer(),
});

interface Props {
  onClose: () => void;
}

function SendInfoScreen(props: Props) {
  return (
    <Screen color={colors.white}>
      <Button title="Close" onPress={props.onClose} color="#b755ff" />
      <AppText
        text="Send Dundies Token"
        style={{
          fontSize: 25,
          fontWeight: "400",
          margin: 15,
          marginBottom: 5,
          color: colors.dark,
          alignSelf: "center",
        }}
      />
      <AppForm<FormValues>
        initialValues={{
          addressOrPhone: "",
          amount: 0,
        }}
        onSubmit={(values) => SMSSender.send(values.addressOrPhone, values.amount.toString())}
        validationSchema={validationSchema}
      >
        <AppFormField
          name="addressOrPhone"
          icon="email"
          otherProps={{
            autoCorrect: false,
            autoCapitalize: "none",
            placeholder: "Phone number or address",
          }}
        />
        <AppFormField
          name="amount"
          icon="square-inc-cash"
          width={200}
          otherProps={{
            autoCorrect: false,
            autoCapitalize: "none",
            placeholder: "Amount",
            keyboardType: "decimal-pad",
          }}
        />

        <SubmitButton title="Send" />
      </AppForm>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {},
});

export default SendInfoScreen;
