import colors from "./app/utils/colors";
import { DefaultTheme } from "@react-navigation/native";

export default {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.primaryDark,
    background: colors.white,
  },
};
